// ==UserScript==
// @name     LR Comments tracker
// @version  1
// @grant    none
// @include  https://www.londonreconnections.com/*
// ==/UserScript==

const trackingPages = JSON.parse(window.localStorage.getItem('DJL-LR-Comments-Tracker') || "{}");
let pollInterval = parseInt(window.localStorage.getItem('DJL-LR-Comments-Tracker-PollInterval')) || 300000; //default: 5 mins
console.log(trackingPages, document.location.pathname.replace(/[/]$/, ''), document.location.pathname.replace(/[/]$/, '') in trackingPages);
let pageIsTracked = document.location.pathname.replace(/[/]$/, '') in trackingPages;
if (pageIsTracked)
{
  //console.log('on a tracked page, updating lastread comment');
  let page = trackingPages[document.location.pathname.replace(/[/]$/, '')];
  let unreadComments = document.querySelectorAll('.commentlist > li#' + page.lastReadComment + ' ~ li');
  //console.log(unreadComments);
  if (unreadComments.length>0) {
  	unreadComments.forEach(el=>{
      //console.log(el.querySelector('div'));
      //el.querySelector('div').style.backgroundColor='#FFC';
      el.classList.add('DJL-LR-Comments-Tracker-New');
    });
  	document.location='#' + unreadComments[0].id.replace(/^li-/, '');
  } else if (page.lastReadComment)
  {
    	document.location='#' + Array.from(document.querySelectorAll('.commentlist>li')).slice(-1)[0].id.replace(/^li-/, '');
  } else {
    document.location='#comments';
  }
  page.lastReadComment = Array.from(document.querySelectorAll('.commentlist > li')).slice(-1)[0].id;
  save();
}

let pollTimeout;
poll();// = setTimeout(poll, pollInterval);


function getTrackedPageInfo(htmlDOM, page, pageInfo = {}, pageUpdated)
{
  //console.log('title', htmlDOM.querySelector('h1.title'), htmlDOM.querySelector('h1.title').innerText);
  pageInfo.title = htmlDOM.querySelector('h1.title').innerText;
  let lastComment = htmlDOM.querySelector('.commentlist > li:last-of-type>div');
  let lastCommentID = lastComment.attributes['id'].value;
  console.log(lastCommentID, pageInfo.lastCommentID, lastCommentID!==pageInfo.lastCommentID, pageUpdated);
  if (lastCommentID!==pageInfo.lastCommentID)
  {
    pageUpdated && pageUpdated({
      page: page, 
      title: pageInfo.title,
      commentHTML: lastComment.innerHTML, 
      commentID: (htmlDOM.querySelectorAll('.commentlist > li#' + pageInfo.lastReadComment + ' ~ li')[0]||{}).id
    });
  }
  pageInfo.lastCommentID = lastCommentID;
  if (pageInfo.lastReadComment)
  {
    pageInfo.firstUnreadComment = (htmlDOM.querySelectorAll('.commentlist > li#' + pageInfo.lastReadComment + ' ~ li')[0]||{}).id;
    pageInfo.unreadCommentCount = htmlDOM.querySelectorAll('.commentlist > li#' + pageInfo.lastReadComment + ' ~ li').length;
  } else {
    pageInfo.firstUnreadComment = (htmlDOM.querySelectorAll('.commentlist > li')[0]||{}).id;
    pageInfo.unreadCommentCount = htmlDOM.querySelectorAll('.commentlist > li').length;
  }
  return pageInfo;
}

function poll(){
  let pages = Object.keys(trackingPages).sort((a,b)=>Math.random()-0.5); // randomise the order we check them in, becuase why not?!
  let updatedPages=[];
  pollPage(0);
  function pollPage(pageNo){
   if (pages.length<=pageNo){
     
     save();
     console.log(updatedPages);
     if (updatedPages.length>0)
     {
     	let updates= parseHTML(`
<div class="DJL-LR-CommentsTracker-Updates">
	<i id="Close" class="material-icons">close</i>
	<ul>${  updatedPages.map(p=>`<li><a href="${p.page}#${p.commentID.replace(/^li-/, '')}"><h1>${p.title}</h1></a><div>${p.commentHTML}<span class="clearfix"></span></div><i class="Dismiss material-icons">close</i></li>`)}
</ul>
</div>`);
       updates.querySelector('#Close').addEventListener('click', e=> { remove(updates);});
       
       console.log(updates);
       updates = console.log(append(document.body,updates));
     }
     
     console.log('Polling complete', pollInterval);
     setTimeout(poll, pollInterval);
     return;
   }
    console.info('polling', pages[pageNo], typeof(pages[pageNo]));
    let page = pages[pageNo];
    fetch(document.location.origin + page).then(resp=>{
      //console.log(resp);
      if (resp.ok){
       return resp.text().then(html=>{
         //console.debug('html', html);
         let htmlDOM = parseHTML(html);
         trackingPages[page] = getTrackedPageInfo(htmlDOM, page, trackingPages[page], obj=>{
           console.log('obj', obj);
           updatedPages.push(obj)
         });
       });
      }
    }).catch(ex=> {
      console.error(ex);
    }).then(()=>pollPage(pageNo+1));
  }
}

function parseHTML(str) {
  var tmp = document.implementation.createHTMLDocument();
  tmp.body.innerHTML = str;
  return tmp;
};

function append(target, src, appendMethod = target.appendChild) {
 if (src instanceof HTMLDocument){
   src = src.body;
 }
 if (src instanceof HTMLBodyElement){
   src = src.childNodes;
 }
 if (src instanceof NodeList){
   src = Array.from(src);
 }
 if (src instanceof HTMLElement || src instanceof Text){
   return append(target, [src], appendMethod)[0];
 }
 if (!Array.isArray(src)){
   throw new Error("don't know how to deal with src type");
 }
 return src.map(el=>
                {
   try {
   appendMethod.apply(target, [el])
   } catch (ex)
   {
     console.error(ex);
   }
 });
}

function remove(src) {
  if (src instanceof HTMLDocument){
   src = src.body;
 }
  if (src instanceof HTMLBodyElement){
   src = src.childNodes;
 }
 if (src instanceof NodeList){
   src = Array.from(src);
 }
 if (src instanceof HTMLElement || src instanceof Text){
   return append(target, [src], appendMethod)[0];
 }
 if (!Array.isArray(src)){
   throw new Error("don't know how to deal with src type");
 }
 return src.map(el=>{
   try {
   el.remove();
   } catch (ex)
   {
     console.error(ex);
   }
 });
}

function setupGUI(){
  let gui = parseHTML(`
<div>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
  <i class="material-icons" id="DJL-LR-CommentsTracker-Icon">track_changes</i>
  <div id="DJL-LR-CommentsTracker-Menu" class="${pageIsTracked?'tracking':''}">
		<i class="material-icons" id="Untrack">remove_circle</i>
	  <i class="material-icons" id="Track">add_circle</i>
    <h1>Tracked pages:</h1>
    <table id="TrackedPages"><tbody><tr><td>Please wait...</td></tr></tbody></table>
  </div>
</div>
`).body.children[0];
  console.log('gui', gui);
  function updateTable() {
    gui.querySelector('#TrackedPages').innerHTML = `
      <thead><tr><th>Title</th><th>Unread comments</th></tr></thead>
      <tbody>
        ${ Object.keys(trackingPages)
                 .map(k=>({url:k,p:trackingPages[k]}))
                 .map(({url,p})=>`<tr ${ url==document.location.pathname.replace(/[/]$/, '')?'class="current"':''}><th><a href="${url}#${(p.firstUnreadComment || p.lastCommentID).replace(/^li-/, '')}">${p.title}</a></th><td>${p.unreadCommentCount||0}</td></tr>`)
         }
      </tbody>`;
    }
  gui.querySelector('#DJL-LR-CommentsTracker-Icon')
     .addEventListener('click', ()=> {
    //console.log('click', gui.querySelector('#DJL-LR-CommentsTracker-Menu'));
    updateTable();
    gui.querySelector('#DJL-LR-CommentsTracker-Menu').classList.toggle('shown');
  });

  gui.querySelector('#Track')
     .addEventListener('click', ()=> {
    console.log('click2');
    pageIsTracked = true;
    gui.querySelector('#DJL-LR-CommentsTracker-Menu').classList.add('tracking');
    trackingPages[document.location.pathname.replace(/[/]$/, '')] = getTrackedPageInfo(document, document.location.pathname.replace(/[/]$/, ''));
    console.log(trackingPages);
    save();
    updateTable();
  });
  gui.querySelector('#Untrack')
     .addEventListener('click', ()=> {
    console.log('click');
    pageIsTracked = false;
    gui.querySelector('#DJL-LR-CommentsTracker-Menu').classList.remove('tracking');
    delete trackingPages[ document.location.pathname.replace(/[/]$/, '')];
    save();
    updateTable();
  });

  append(document.body, gui);
	
  fetch('http://www.dj-djl.com/LRCommentsTracker.css').then(resp=>{
    if (resp.ok)    {
      return resp.text().then(css=>{
        let style = document.createElement('style');
        style.innerText = css;
        document.body.appendChild(style);
      });
    }
  });
}
setupGUI();

function save(){
window.localStorage.setItem('DJL-LR-Comments-Tracker', JSON.stringify(trackingPages));
}

